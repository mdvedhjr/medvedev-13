#include <iostream>
#include <algorithm>

using namespace std;

void FillMatrix(int Matrix[6][6]) {
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 6; j++) {
			Matrix[i][j] = rand() % 37 - 17;
		}
	}
}

void MaxOnDiagonal(int Matrix[6][6]) {
	int temp = -18;
	for (int i = 0; i < 6; i++) {
		temp = max(temp, Matrix[i][i]);
	}
	cout << "Max on main diagonal = " << temp << endl;

}

int main() {

	int Matrix[6][6];

	FillMatrix(Matrix);

	MaxOnDiagonal(Matrix);

}