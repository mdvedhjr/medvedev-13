#include <iostream>
#include <algorithm>

using namespace std;

void FillMatrix(int Matrix[6][6]) {
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 6; j++) {
			Matrix[i][j] = rand() % 37 - 17;
		}
	}
}

int MaxOnDiagonal(int Matrix[6][6]) {
	int temp = -18;
	int pos;
	for (int i = 0; i < 6; i++) {
		if (temp < Matrix[i][i]) {
			temp = Matrix[i][i];
			pos = i;
		}
	}
	cout << "Max on main diagonal = " << temp << endl;
	return pos;
}

pair<int, int> MaxUnderDiagonal(int Matrix[6][6]) {
	int temp = -18;
	pair<int, int> pos;
	for (int i = 1; i < 6; i++) {
		for (int j = 0; j <= i; j++) {
			if (temp < Matrix[i][j]) {
				temp = Matrix[i][j];
				pos = { i, j };
			}
		}
	}
	cout << "Max under main diagonal = " << temp << endl;
	return pos;
}

void PrintMatrix(int Matrix[6][6]) {
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 6; j++) {
			cout.width(2);
			cout << Matrix[i][j] << ' ';
		}
		cout << '\n';
	}
}

void SwapMaximums(int Matrix[6][6]) {
	pair<int, int>  pos2;
	int pos1;
	pos1 = MaxOnDiagonal(Matrix);
	pos2 = MaxUnderDiagonal(Matrix);
	Matrix[pos1][pos1] = Matrix[pos2.first][pos2.second];
}

int main() {

	int Matrix[6][6];

	FillMatrix(Matrix);

	SwapMaximums(Matrix);

	PrintMatrix(Matrix);
}